Cliquez en haut à droite sur le bouton bleu Clone, et copiez la deuxième ligne de la pop-up Clone with HTTPS.

Prenez ensuite une console d'ordinateur, représentant votre poste de développeur, et clonez le repository en tapant la commande suivante :

git clone https://gitlab.com/[votre-nom-d-utilisateur]/spring-petclinic-microservices.git
cd spring-petclinic-microservices

----------------------
Une fois le clone fait, nous nous retrouvons avec un dossier vide où la branche master est associée à notre repository GitLab.
Nous allons ajouter une branche upstream sur GitHub et puller les dernières modifications GitHub. 
Les commandes à taper sont les suivantes :

git remote add upstream https://github.com/spring-petclinic/spring-petclinic-microservices.git
git pull upstream master
git push origin master
----------------------------
Normalement, si les commandes précédentes ont été exécutées, le dossier Git devrait contenir le code source du projet PetClinic, ainsi que toutes les modifications associées.

***************

Dans un premier temps, créez une nouvelle branche qui va accueillir nos modifications :

git checkout -b refactor-customers

----------------------
Ensuite, éditez le fichier Java "spring-petclinic-customers-service/src/main/java/org/springframework/samples/petclinic/customers/CustomersServiceApplication.java". Supprimez par exemple le " ; " situé à la fin de la lignepackage org.springframework.samples.petclinic.customers et commitez les changements dans Git :

git add spring-petclinic-customers-service/src/main/java/org/springframework/samples/petclinic/customers/CustomersServiceApplication.java
git commit -m "Refactorisation du code des clients"
git push origin refactor-customers
----------------------------

Vous devriez maintenant avoir deux branches visibles sur la page d'accueil du projet.

***************** Clone only one branch -----
git clone -b mybranch --single-branch git://sub.domain.com/repo.git 
----
git clone -b refactor-customers  --single-branch   https://gitlab.com/macrosoftas/spring-petclinic-microservices

-------------------------------------

I have done with below single git command:

git clone [url] -b [branch-name] --single-branch
